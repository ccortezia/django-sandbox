import logging
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello, world. You're at the sand index.")

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.decorators.csrf import requires_csrf_token, csrf_exempt
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import user_passes_test, login_required

logger = logging.getLogger(__name__)

def is_reader(user):
    return user.has_perm('sand.view_horse')

def is_writer(user):
    return user.has_perm('sand.change_horse')

@user_passes_test(is_reader)
def list_horses_view(request):
    return HttpResponse('ok ' + str(request.user.get_group_permissions()))

@user_passes_test(is_writer)
def change_horse_view(request):
    return HttpResponse('ok ' + str(request.user.get_group_permissions()))

@login_required
def reset_password_view(request):
    user = User.objects.get(username__exact=request.user.username)
    user.set_password('default')
    user.save()
    return HttpResponse("password set to default for user %s" % user.username)

@csrf_exempt
def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    
    if request.user.username == username and request.user.is_authenticated():
        return HttpResponse("user %s already logged in" % request.user.username)

    logger.debug("authenticate ...")
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            logger.debug("before login")
            login(request, user)
            logger.info("success")
            return HttpResponse("Success login")
        else:
            # Return a 'disabled account' error message
            logger.error("disabled account")
            raise PermissionDenied
    else:
        # Return an 'invalid login' error message.
        logger.error("invalid login")
        raise PermissionDenied

    return HttpResponse("Don't know how I got here")

def logout_view(request):
    logout(request)
    return HttpResponse("Successfully logged out")