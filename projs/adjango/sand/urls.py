from django.conf.urls import patterns, url

from sand import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login$', views.login_view, name='login_view'),
    url(r'^reset_password$', views.reset_password_view, name='reset_password_view'),
    url(r'^logout$', views.logout_view, name='logout_view'),
    url(r'^horses$', views.list_horses_view, name='list_horses_view'),
    url(r'^horse/update$', views.change_horse_view, name='change_horse_view'),
)